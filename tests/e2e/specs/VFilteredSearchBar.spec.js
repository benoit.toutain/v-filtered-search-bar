// https://docs.cypress.io/api/introduction/api.html

describe('FilteredSearchBar', {testIsolation: false}, () => {
  beforeEach(() => {
    cy.intercept('https://api.coindesk.com/v1/bpi/currentprice.json', {fixture: 'currency.json'})
      .as('getCurrency')
    cy.intercept('https://calendrier.api.gouv.fr/jours-feries/metropole/2021.json', {fixture: 'fairydays.json'})
      .as('getFairyDays')
    cy.visit('/')
    cy.get('[data-e2e=cancel]')
      .should('be.visible')
      .click()
  })

  it('Has a filtered search bar', () => {
    cy.get('.filtered-search-bar')
      .should('be.visible')
  })

  it('Has a cancel button', () => {
    cy.get('[data-e2e=cancel]')
      .should('be.visible')
  })

  it('Has a prepend icon', () => {
    cy.get('.mdi-magnify')
      .should('be.visible')
  })

  it('Does not display any search criteria', () => {
    cy.get('.v-chip-group')
      .should('have.length', 0)
  })

  it('Displays a menu when we click in the input', () => {
    cy.get('[data-e2e=v-text-field')
      .click()
    cy.get('.v-list')
      .should('be.visible')
  })

  it('Hides the menu when clicking outside', () => {
    cy.get('[data-e2e=v-text-field]')
      .click()
    cy.get('.v-list')
      .should('be.visible')
    cy.get('body')
      .click()
    cy.get('.v-list > :first-child')
      .should('not.be.visible')
  })

  it('Filters the menu items when we enter some chars', () => {
    cy.get('[data-e2e=v-text-field]')
      .click()
    cy.get('body').type('aut')
    cy.get('.v-list > *')
      .should('have.length', 1)
  })

  it('Displays a select search criteria when clicking on a menu item', () => {
    cy.get('[data-e2e=v-text-field]')
      .click()
    cy.get('.v-list > :first-child')
      .click()
    cy.get('.v-chip')
      .should('have.length', 2)
    cy.get('.v-chip:last-of-type')
      .should('contain', '=')
  })

  it('Displays a select search criteria when pressing Enter and there is only one item left in the menu', () => {
    cy.get('[data-e2e=v-text-field]')
      .click()
    cy.get('[data-e2e=v-text-field]').type('author')
    cy.get('[data-e2e=v-text-field]').type('{enter}')
    cy.get('.v-chip')
      .should('have.length', 2)
    cy.get('.v-chip:last-of-type')
      .should('contain', '=')
  })

  it('Displays the search value when we search against a free text type search criteria', () => {
    cy.get('[data-e2e=v-text-field]')
      .click()
    cy.get('[data-e2e=v-text-field]').type('free')
    cy.get('[data-e2e=v-text-field]').type('{enter}')
    cy.get('[data-e2e=v-text-field]').type('AZERTY')
    cy.get('[data-e2e=v-text-field]').type('{enter}')
    cy.get('.v-chip')
      .contains('AZERTY')
      .should('be.visible')
  })

  it('Does not display a select search criteria when pressing Enter and there is more than one item left in the menu', () => {
    cy.get('[data-e2e=v-text-field]')
      .click()
    cy.get('[data-e2e=v-text-field]').type('{enter}')
    cy.get('.v-chip')
      .should('have.length', 0)
  })

  it('Displays a select search criteria when selecting a menu item with arrows', () => {
    cy.get('[data-e2e=v-text-field]')
      .click()
    cy.get('body').type('{downarrow}')
    cy.get('body').type('{enter}')
    cy.get('.v-chip')
      .should('have.length', 2)
    cy.get('.v-chip:last-of-type')
      .should('contain', '=')
  })

  it('Removes one item when pressing two times on Backspace', () => {
    cy.get('[data-e2e=v-text-field]')
      .click()
    cy.get('body').type('{downarrow}')
    cy.get('body').type('{enter}')
    cy.get('body').type('{enter}')
    cy.get('[data-e2e=v-text-field]')
      .click()
    cy.get('[data-e2e=v-text-field]').type('{backspace}')
    cy.get('[data-e2e=v-text-field]').type('{backspace}')
    cy.get('.v-chip')
      .should('have.length', 0)
  })

  it('Removes the value of the filter when pressing once on Backspace', () => {
    cy.get('[data-e2e=v-text-field]')
      .click()
    cy.get('body').type('{downarrow}')
    cy.get('body').type('{enter}')
    cy.get('body').type('{downarrow}')
    cy.get('body').type('{enter}')
    cy.get('.v-chip')
      .should('have.length', 3)
    cy.get('[data-e2e=v-text-field]').type('{backspace}')
    cy.get('.v-chip')
      .should('have.length', 2)
    cy.get('.v-chip:last-of-type')
      .should('contain', '=')
  })

  it('Removes all items when clicking the cancel button', () => {
    cy.get('[data-e2e=v-text-field]')
      .click()
    cy.get('body').type('{downarrow}')
    cy.get('body').type('{enter}')
    cy.get('body').type('{downarrow}')
    cy.get('body').type('{enter}')
    cy.get('.v-chip')
      .should('have.length', 3)
    cy.get('[data-e2e=cancel')
      .click()
    cy.get('.v-chip')
      .should('have.length', 0)
  })

  it('Allows to chain filters', () => {
    cy.get('[data-e2e=v-text-field]')
      .click()
    cy.get('.v-list > :first-child')
      .click()
    cy.get('.v-list > :first-child')
      .last()
      .click()
    cy.get('.v-list > :first-child')
      .last()
      .click()
    cy.get('.v-list > :first-child')
      .last()
      .click()
    cy.get('.v-list > :first-child')
      .last()
      .click()
    cy.get('.v-list > :first-child')
      .last()
      .click()
    cy.get('.v-chip')
      .should('have.length', 9)
  })

  it('Displays a scroll if numerous filters are set', () => {
    cy.get('[data-e2e=v-text-field]')
      .click()
    for (let i = 0; i < 20; i++) {
      cy.get('.v-list > :first-child')
        .last()
        .click()
    }
    cy.get('.v-chip:first')
      .should('not.be.visible')
    cy.get('.v-chip:last')
      .should('be.visible')

  })

})
