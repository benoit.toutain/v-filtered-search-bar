import {defineConfig} from 'cypress'

export default defineConfig({
  projectId: "",
  video: true,
  screenshotOnRunFailure: true,
  retries: {
    runMode: 2,
    openMode: 0
  },
  defaultCommandTimeout: 10000,
  fixturesFolder: 'tests/e2e/fixtures',
  screenshotsFolder: 'tests/e2e/screenshots',
  videosFolder: 'tests/e2e/videos',
  e2e: {
    experimentalMemoryManagement: true,
    numTestsKeptInMemory: 50,
    specPattern: "tests/e2e/**/*.spec.{js,jsx,ts,tsx}",
    baseUrl: "http://localhost:8080/#",
    supportFile: 'tests/e2e/support/index.js',
    testIsolation: true,
    setupNodeEvents(on, config) {
    }
  }
})
