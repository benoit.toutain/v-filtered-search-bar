import axios from 'axios'
import {JSONPath} from 'jsonpath-plus'

export function useDataFromApi() {

  function getDataFromApi(items,token) {
    let headers = {
      Accept: 'application/json'
    }
    if (token !== '') {
      headers = {
        ...headers,
        Authorization: 'Bearer ' + token
      }
    }
    if (!Array.isArray(items)) {
      let api = items.api
      return axios.get(api, {
        headers
      }).then(response => {
        let toR = []
        items.fields.forEach(field => {
          let result = JSONPath(field.jsonpath, response.data)
          if (toR.length === 0) {
            for (let i = 0; i < result.length; i++) {
              let data = {}
              data[field.name] = result[i]
              toR.push(data)
            }
          } else {
            for (let i = 0; i < result.length; i++) {
              toR[i][field.name] = result[i]
            }
          }
        })
        return Promise.resolve(toR)
      }).catch(rejected => {
        console.log(rejected)
      })
    } else {
      return items
    }
  }

  return {getDataFromApi}
}