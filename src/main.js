/**
 * main.js
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Components
import App from './App.vue'

// Composables
import { createApp } from 'vue'

import vuetify from './plugins/vuetify'
import myPlugin from './plugins/index'


const app = createApp(App)

app.use(vuetify)
app.use(myPlugin)

app.mount('#app')
