export default {
  default: {
    dark: false,
    colors: {
      primary: '#1976D2',
      secondary: '#424242',
      accent: '#82B1FF',
      error: '#FF5252',
      info: '#2196F3',
      success: '#4CAF50',
      warning: '#FB8C00',
    },
  },
  defaultDark: {
    dark: true,
    colors: {
      primary: '#2196F3',
      secondary: '#424242',
      accent: '#FF4081',
      error: '#FF5252',
      info: '#2196F3',
      success: '#4CAF50',
      warning: '#FB8C00',
    }
  },
  aspar: {
    dark: false,
    colors: {
      primary: '#32161F',
      secondary: '#775B59',
      accent: '#392F5A',
      error: '#F57A00',
      info: '#FFF8F0',
      success: '#2E766D',
      warning: '#F4D06F'
    }
  },
  asparDark: {
    dark: true,
    colors: {
      primary: '#AA4B69',
      secondary: '#775B59',
      accent: '#392F5A',
      error: '#F57A00',
      info: '#FFF8F0',
      success: '#45B0A4',
      warning: '#F4D06F'
    }
  }
}
