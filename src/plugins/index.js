import Component from '../components/VFilteredSearchBar.vue'

const defaultOptions = {
  /**
   * A function to get a JWT Token, in case the API url
   * to search values needs it.
   */
  getToken: () => ''
}

// This exports the plugin object.
export default {
  install(app, options) {

    // merge default options with arg options
    let userOptions = {...defaultOptions, ...options};

    // provide getToken function to be used in v-filtered-search-bar component
    app.provide('getToken', userOptions.getToken)

    app.component('v-filtered-search-bar', Component)

  }
}
